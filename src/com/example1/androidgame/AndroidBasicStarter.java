package com.example1.androidgame;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.app.Activity;
import android.widget.ListView;
import android.view.Menu;

public class AndroidBasicStarter extends ListActivity {
	String tests[] = {"LifeCycleTest", "singleTouchTest","MultiTouchTest",
			"KeyTest","AccelerometerTest","AssetsTest",
			"ExternalStorageTest", "SoundPoolTest","MediaPlayerTest",
			"FullScreenTest","RenderViewTest","ShapeTest","BitmapTest",
			"FontTest","SurfaceViewTest"};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_android_basic_starter);
		setListAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, tests));
	} 

	protected void onListItemClick(ListView list, View view, int position, long id){
		super.onListItemClick(list, view, position, id);
		String testName = tests[position];
		
		try{
			Class clazz = Class.forName("com.badlogic.androidgames." + testName);
			Intent intent = new Intent(this, clazz);
			startActivity(intent);
			
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.android_basic_starter, menu);
		return true;
	}

}
